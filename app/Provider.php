<?php

namespace App;

use App\Interfaces\CarrierInterface;
use App\Interfaces\CallInterface;
use App\Services\ContactService;

use App\Contact;
use App\Call;

class Provider implements CarrierInterface
{

	public function dialContact(Contact $contact) {
    return 'dial to '.$contact->getName();
  }

	public function makeCall(): Call {
    return new Call();
  }

  public function sendSMS($number, $body) {
    return 'send SMS to '.$number.' with body: '.$body;
  }
}

<?php

namespace App;

use App\Interfaces\CarrierInterface;
use App\Services\ContactService;

use App\Contact;

class Mobile
{

	protected $provider;
	
	function __construct(CarrierInterface $provider)
	{
		$this->provider = $provider;
	}


	public function makeCallByName($name = '')
	{
		if( empty($name) ) return;

		$contact = ContactService::findByName($name);

		if($contact->getName() === '') {
			return null;
		}

		$this->provider->dialContact($contact);

		return $this->provider->makeCall();
	}

	// Funcion para enviar SMS por numero
	public function sendSMSByNummber($number = '', $bodySMS = '')
	{
		if( empty($number) ) return null;

		$isValid = ContactService::validateNumber($number);

		if ($isValid) {
			$contact = new Contact('', $number);
			$this->provider->dialContact($contact);
			return $this->provider->sendSMS($number, $bodySMS);
		}
		else {
			return null;
		}
	}
}

<?php

namespace App\Services;

use App\Contact;


class ContactService
{
	
	public static function findByName(string $name): Contact
	{
		// lista de contacto de prueba
		$listContact = array(0 => 'Diego', 1 => 'Natalia', 2 => 'Jose', 3 => 'Juan');

		// buscando contacto dentro de la lista
		// si: existe el contacto
		if(in_array($name, $listContact)) {
			$contact = new Contact($name);
			return $contact;
		}
		// no: no existe contacto en la lista
		else {
			return new Contact();
		}
	}

	public static function validateNumber(string $number): bool
	{
		$listContact = array(0 => '925487555', 1 => '995548245', 2 => '987548550', 3 => '957848562');

		// validando el tamaño del numero
		if(strlen($number) != 9) {return  false;}
		
		// validando que sea numero entero.
		$numberInt = intval($number);
		if($numberInt == 0){ return false;}

		// numero valido
		// si: existe el contacto
		if(in_array($number, $listContact)) {
			return true;
		}
		// no: no existe contacto en la lista
		else {
			return false;
		}
	}
}
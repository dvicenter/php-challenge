<?php

namespace Tests;

use Mockery as m;
use PHPUnit\Framework\TestCase;

use App\Mobile;
use App\Provider;


class MobileTest extends TestCase
{
	
	/** @test */
	public function it_returns_null_when_name_empty()
	{
		$provider = new Provider();
		$mobile = new Mobile($provider);

		$this->assertNull($mobile->makeCallByName(''));
	}

	/** @test */
	public function it_not_returns_null_when_name()
	{
		$provider = new Provider();
		$mobile = new Mobile($provider);

		$this->assertNotNull($mobile->makeCallByName('Diego'));
	}

	/** @test */
	public function it_not_exist_contact()
	{
		$provider = new Provider();
		$mobile = new Mobile($provider);

		$this->assertNull($mobile->makeCallByName('Carlos'));
	}

	/** @test */
	public function it_send_sms_by_number()
	{
		$provider = new Provider();
		$mobile = new Mobile($provider);

		$this->assertNotNull($mobile->sendSMSByNummber('925487555'));
	}

}
